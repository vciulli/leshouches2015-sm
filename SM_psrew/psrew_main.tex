\section{Reweighting the \texorpdfstring{S\protect\scalebox{0.8}{HERPA}}{SHERPA} parton shower \texorpdfstring{\protect\footnote{E.\ Bothmann, M.\ Sch\"onherr, S.\ Schumann}}{}}
\label{sec:psrew}

\subsection{Introduction}

The last ten years of improving Monte-Carlo event generators (MCEG) enabled
quantitative predictions at an unprecedented accuracy. These developments 
led to a considerable growth in computational cost per event. This can become
limiting, as calculations must often be performed repeatedly, 
e.g.\ to assess theory uncertainties. For QCD calculations such 
re-calculations are necessary for each combination of QCD input parameters. 
Those include parton density functions (PDF), the strong coupling 
($\alpha_S$) or the renormalisation/factorisation scale. This adds up 
to $\mathcal{O}(10^3)$ re-calculations quickly. For PDF fits, the 
demand for re-calculations is even higher.

This issue is addressed by \emph{not doing the whole calculation again}.
Instead, parameter-independent bits of the calculation are stored
as weights and can then be recombined with different parameter values.
This can either be done directly for each event,
or after projecting weights to interpolation grids first,
with one grid per observable bin and dependence
structure~\cite{Carli:2010rw,Kluge:2006xs}.
PDF fitters use such interpolation grids for cross section predictions,
as the time needed for a recombination is of the order of milliseconds only.

For leading-order (LO) events, getting the \emph{parameter-indendent weight} is easy:
divide the event weight
by the PDFs $f_a f_b$ of the incoming partons
and by $\alpha_S^p$, with $p$ being the perturbative order of the process.
Then multiply with $f'_a f'_b \alpha_S'^p$ to get the new event weights
for a different set of input PDFs $f'$ and a different value of the strong 
coupling $\alpha_S'$. For the proper evaluation of these {\em variational
weights} the partonic momentum fractions $x_{1}$ and $x_{2}$ as well as
the factorisation and renormalisation scale need to be book-kept.
At next-to-leading order (NLO)
the procedure is more complicated, because of the additional scale-dependence 
of the virtual corrections and the different kinematics and initial states of the 
subtraction parts~\cite{Bern:2013zja}.

The newly added internal reweighting of \textsc{Sherpa}~\cite{Gleisberg:2008ta}
as of version 2.2.0 performs \emph{on-the-fly}
during event generation~\cite{Bothmann:2015woa}. The result for each combination is inserted into the
{\small \tt HepMC::WeightContainer} object and thus can be either saved to disk,
programmatically accessed or directly passed via the internal interface to the
{\small \tt Rivet} analysis framework~\cite{Buckley:2010ar}.
The supported event generation modes are (N)LO, (N)LO+PS (i.e. matched to a parton shower)
and MEPS@LO (i.e. merged LO matrix elements with a shower).
The recently introduced interfaces {\small\tt MCgrid}~\cite{DelDebbio:2013kxa} and
{\small\tt aMCfast}~\cite{Bertone:2014zva} on the other hand
allow for the automated creation of interpolation grids using
general MCEGs. (N)LO and (N)LO+PS are supported.

For both reweighting approaches, event generations
involving a parton shower come with a caveat:
The shower takes no part
in the reweighting procedure, and is kept as-is. Its dependences
on PDF, scale and $\alpha_S$ are thus not taken into account.
Therefore the reweighting leads to an \emph{inconsistent result},
where dependences in the hard process have been updated,
but not in the shower.
There are indications that this does not affect major parts of
phase space~\cite{Buckley:2016caq,Gieseke:2004tc}.
A more thorough study is however still missing.
We address this by presenting ongoing work to extend the internal 
\textsc{Sherpa} reweighting to include all dependences of the 
parton shower emissions, using a property of the
veto algorithm that allows for a simple multiplicative reweighting.

\subsection{Reweighting the veto algorithm}

The default shower of \textsc{Sherpa}, \textsc{Csshower} \cite{Schumann:2007mg}, 
uses the veto algorithm to
numerically integrate the Sudakov form factors
\begin{equation}
  \Delta(t_0, t_c) = e^{- \int_{t_c}^{t_0} \Gamma(t) \mathrm{d} t}\,,
\end{equation}
which give the no-branching probabilities between the starting scale
$t_0$ and the cut-off scale $t_c$.
In the veto algorithm the splitting kernels $\Gamma$ are replaced with
integrable overestimates $\hat{\Gamma}$. This is balanced by only accepting a
proposed emission with the probability $P_\mathrm{acc} = \Gamma / \hat{\Gamma}$.
A multiplicative factor in $\Gamma$ is therefore equivalent to a
multiplicative factor in $P_\mathrm{acc}$~\cite{Hoeche:2009xc}.
This observation is for example used to apply matrix element 
corrections~\cite{Hoeche:2011fd}, where the splitting kernels are replaced 
with a real-emission-like kernel $R/B$. This is done a-posteriori, i.e. the event weight is multiplied
by $(R/B)/\Gamma$. The emission itself is unchanged.
The same method is also used in the {\small\tt Vincia} parton shower to calculate
uncertainty variations for different scales, finite terms of the antenna functions,
ordering parameters and sub-leading colour corrections~\cite{Giele:2011cb}.

The emission kernels $\Gamma$ depend linearly on $\alpha_S$ and
on a ratio of PDF values $f_a(x/z)/f_b(x)$\footnote{The exact definition of
the flavours $a$, $b$ and the momentum fractions $x$, $x/z$ depend on the
dipole configuration of the emission. There is no PDF dependence in the case
of dipoles that consist only of final-state partons.}.
A change of PDFs $f \rightarrow f'$ and the strong coupling
$\alpha_S \rightarrow \alpha_S'$
is equivalent to modifying the emission probability
accordingly\footnote{Both $\alpha_S$ and $f$ depend on the
emission scale. Although the emission scales can not be
reweighted themselves using the presented method, the functional
form of these dependences can be changed.}:
\begin{equation}
  P_\mathrm{acc} \rightarrow q\,P_\mathrm{acc}\,,\qquad
  q \equiv \frac{\alpha_S'}{\alpha_S} \cdot
  \frac{f'_a(x/z)/f'_b(x)}{f_a(x/z)/f_b(x)}\,,
\end{equation}
i.e. we need to multiply the event weight for each accepted emission by the 
corresponding $q$ in order to reweight the event with a new choice of PDFs.
For the rejected emissions we obtain
\begin{equation}
  P_\mathrm{rej} = 1 - P_\mathrm{acc} \rightarrow 1 - q P_\mathrm{acc}
  = \left[ 1 + \left( 1 - q \right) \frac{P_\mathrm{acc}}{1 - P_\mathrm{acc}}
    \right]\,P_\mathrm{rej}\,,
\end{equation}
so for each of those we need to multiply the event weight with the expression
in square brackets.

Our current implementation supports PDF and $\alpha_S$ reweighting of parton shower emissions for
both LO+PS and NLO+PS events.
One can choose a maximum number
of reweighted emissions per event.
This is useful because some observables are
sensitive only to the first few emissions.
The reduced amount of reweighting per event then
allows for faster event generations.


\subsection{Validation}

We present a selection of plots to validate our implementation.
All contain comparisons between ``dedicated'' and ``reweighted'' results.
The ``dedicated'' results are produced through normal runs for the varied
input parameters, whereas the ``reweighted'' ones are obtained by reweightings
from a run with a central parameter choice.
The reweighting is done for the matrix element exclusively (``ME''),
including all parton shower emissions (``ME+PS'') or just
the first emission (``ME+PS(1st em.)'').

In Fig.~\ref{psrew_fig:appl}, we present uncertainty bands for PDF and 
$\alpha_S$ variations. The left-hand plot features a 
{\small\tt CT14nlo}~\cite{Dulat:2015mca} PDF error band for the $W$-boson
transverse momentum distribution in $W$ production
at the LHC at NLO+PS. The band is a combination of the central member and 56
Hessian eigenvector members. So for the ``dedicated'' band,
57 independent event generation runs had to be performed,
whereas each of the reweighting bands is generated by
a single run.\footnote{We are yet to do a detailed timing
study (and to optimise the code), but for the case of doing 57 variations
and reweighting the ME and up to one parton shower emission,
the run took about a factor of 6 longer than a run
without any reweighting.
% NOTE: For reweighting all emissions, I've found a factor of ~13
Comparing the time needed to generate the entire error band,
the reweighting run thus needed one order of magnitude
less than the combination of dedicated runs.}

A few observations can be made for the $W$ $p_\perp$ plot: (i) For
$p_\perp < 10$ GeV, solely reweighting the ME underestimates 
the error found with the dedicated calculation by about 4\,\% 
in both directions. The ME+PS reweighting is able to reproduce the error.
(ii) For $p_\perp$ values between 20\,GeV and the $W$~mass,
the ME reweighting \emph{overestimates} the positive error slightly,
by about 1\,\%. Again, the ME+PS reweighting correctly
reproduces the error.
(iii) The reweighted bands are much smoother because the prediction
for each PDF member shares the statistics of the central run.
Unfortunately there is no way to guarantee this for the dedicated
runs when varying input parameters of the parton shower,
which makes closure tests between reweighting and dedicated runs a
statistical exercise.

The right-hand plot in Fig.~\ref{psrew_fig:appl}
contains a band for an $\alpha_S$ variation for the thrust
observable for $e^+e^- \to q\bar{q}$ at LEP at LO+PS.
The variations considered around the central values of 
$\alpha_S(m_Z)=0.120$ include $\alpha_S(m_Z)=0.108$ and 
$\alpha_S(m_Z)=0.128$. The running of $\alpha_S$ is evaluated 
at the one-loop order. The plot shows the envelope for the three values.
The reweighted band is obtained by reweighting
all emissions in the run with the central $\alpha_S$ value.
It reproduces the dedicated band within statistical errors.
For this we note that the variance of the reweighted prediction
increases. In some bins, the sum of squared weights is a factor
of 100 higher compared to the dedicated result.

Jet resolutions are sensitive to the specifics of parton shower emissions,
which is why we have in Fig.~\ref{psrew_fig:nrew} a series of closure tests
for this observable for $W$-boson production at the LHC at LO+PS.
The reweightings are done from the central {\small\tt CT14}
to the central {\small\tt MMHT2014} set~\cite{Harland-Lang:2014zoa}.
The maximum number of shower emissions
being reweighted is varied between 0, 1 and $\infty$.
The corresponding reweighted
predictions are compared to the dedicated {\small\tt MMHT2014} result.
This is done for the $0\to1$ and for the $3\to4$ jet resolution.
We observe deviations for the ME-only reweighting prediction of about $\pm2\,\%$.
The 1-emission-reweighting fixes the deviation for $d_{01} > 10$ GeV, but can not
fix the deviations elsewhere. The all-emissions-reweighting is perfect within statistics
for all bins of both jet resolutions.


\begin{figure}[htbp]
\centering
\includegraphics[width=0.49\textwidth]{psrew_W_pT}
\includegraphics[width=0.49\textwidth]{psrew_Thrust}
\caption{A PDF variation for the $W$-boson $p_\perp$ distribution at the LHC 
  at NLO+PS (left) and an $\alpha_S$ variation for the Thrust for $q\bar{q}$ 
  production at LEP at LO+PS (right). Predictions generated by single 
  reweighting runs (``rew'd'') are compared to predictions from dedicated runs.}
\label{psrew_fig:appl}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[width=0.49\textwidth]{psrew_log10_d_01}
\includegraphics[width=0.49\textwidth]{psrew_log10_d_34}
\caption{Predictions for jet resolutions for $W$-boson productions at the LHC 
  at LO+PS. Results from reweighting runs {\small\tt CT14} $\to$ {\small\tt MMHT2014} 
  PDF are compared to the dedicated result for direct use of the {\small\tt MMHT2014} 
  PDF.}
\label{psrew_fig:nrew}  
\end{figure}

\subsection{Conclusions}

We have presented the extension of the internal reweighting of \textsc{Sherpa}
to include the $\alpha_S$ and PDF dependences of parton shower emissions.
It has been shown that the ME-only reweighting has shortcomings compared to the newly
implemented full reweighting.
We did this by comparing theory uncertainty bands generated by reweighting runs
with dedicated calculations for $\alpha_S$ and PDF variations at LO+PS and (N)LO+PS.
Now with the support for these input parameters in place, the next step is to
allow for varying the scales at which $\alpha_S$ and the PDFs are evaluated
for a given emission. This is different from varying the actual emission scale
(e.g.\ indirectly by choosing a new starting scale), which is not covered
by the presented reweighting method.

The full direct reweighting for parton showers
will leave us with a complete picture of
how to reweight when all information is still available.
This will help us when bringing parton shower reweighting
also to grids, where individual event information is \emph{not} available.
For this we are considering an approximate approach.
Such shower-aware grids would allow for including more data in PDF fits,
namely data that is currently omitted because its theoretical prediction
is sensitive to higher-order corrections.

\subsection*{Acknowledgements}
We acknowledge financial support from BMBF under contract 05H15MGCAA, 
the Swiss National Foundation (SNF) under contract PP00P2--128552 and 
from the EU MCnetITN research network funded under Framework Programme 
7 contract PITN--GA--2012--315877.

