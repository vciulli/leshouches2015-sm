#!/bin/sh

for contribution  in SM_*/*_section.tex SM_*/*_chapter.tex
do
  cd ${contribution%%/*}
  tmp=${contribution##*/}
  pdflatex $tmp
  bibtex ${tmp%%.tex}
  pdflatex $tmp
  cd ../
done
