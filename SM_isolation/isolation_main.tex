\section{Photon isolation studies
  \texorpdfstring{\protect\footnote{
    L.~Cieri, G.~Heinrich
  }}{}}

We study the  effect of various photon isolation criteria in processes
involving single photon plus jet and diphoton production.

\subsection{Introduction}

 Final states involving isolated photons have played and still do play
 a very important role at the LHC. Due to the clean photon signal, 
the diphoton channel has been the discovery channel for the Higgs
boson, and it keeps intriguing us by exhibiting structures that may
hint to resonances existing around 750\,GeV.

Even within the Standard Model, photons in the final state can have
a number of  different origins.
They can either originate directly from the hard interaction
(in this case they are called {\it direct}), or they can come from a
fragmentation process of a QCD parton in the final state.
Further there is a (large but reducible) background of secondary photons 
coming from the decay of pions, $\eta$-mesons, etc.
In order to suppress the background, an 
 {\it isolation criterion} is usually applied, which suppresses both
 the secondary photons and the fragmentation component.

Beyond the leading order, the distinction between the direct and
the fragmentation contribution is not unique any more. 
For example, at NLO, the collinear splitting of a parent quark into a 
photon and quark leads to a singularity, which can be absorbed into
fragmentation functions, analogous to the absorption of initial
state QCD radiation into ``bare PDFs''. 
The fragmentation functions have a perturbative component, 
involving the splitting function $P_{q\to \gamma+q}(x)$, and a
non-perturbative component which has to be obtained from fits to data.
As these fits have been done based on LEP data, and are less refined than 
the PDF fits which are based on a wealth of data and which are constantly
improved, 
there is an inherent uncertainty in the fragmentation component which 
makes it desirable to suppress this contribution as much as possible. 
Another reason to suppress it, apart from the obvious experimental reasons, 
is given by the fact that higher
orders for this part are much more difficult to calculate than 
for the direct photon contribution, as it requires the calculation of 
jet cross sections, which are then convoluted with the fragmentation functions.

In the following we will study the behaviour of the direct and fragmentation
contributions with different isolation criteria and at different
orders in perturbation theory, not only for
diphotons, but also for photon plus jet final states. 
We will also study how the effects vary with the centre of mass energy 
by comparing results for $\sqrt{s}=8, 14$ and 100 TeV.
 
\subsection{Isolation criteria}

The  most commonly used isolation criterion at hadron collider 
experiments is a {\it cone-based isolation} prescription.
In this procedure, the photon candidate is called isolated if
in a cone of radius $R$ in rapidity $y$ and azimuthal angle $\phi$ around the
photon direction, 
the amount of  hadronic transverse energy $\sum E_{T}^{had}$
is smaller than some value 
$E_{T}^{max}$ chosen by the experiment:
\begin{eqnarray}\label{eq:coneisol}    
&\sum E_{T}^{had} \leq E_{T}^{max} \nonumber\\
&\mbox{inside a cone with}     
\left( y - y_{\gamma} \right)^{2} +    
\left(  \phi - \phi_{\gamma} \right)^{2}  \leq R^{2}  \;.   
\end{eqnarray} 
$E_{T}^{max}$ can be defined either in absolute terms, or as 
 fraction $\eps_c$ of $p_{T}^{\gamma}$ (typically $\eps_c$= 0.1 or below).  

\vspace*{4mm}

In a theoretical calculation, the need for a fragmentation contribution 
can be eliminated by using a {\it smooth  isolation}
criterion~\cite{Frixione:1998hn}, where
the  threshold on the hadronic energy inside the isolation cone
decreases with the
 radial distance from the photon. It is described by the cone size $R$, 
 a weight factor $n$ and 
 an isolation parameter $\epsilon_f$. With this criterion, the
 photon is isolated if
% if the energy in any sub-cone does not exceed
 \begin{eqnarray}\label{eq:frixisol}     
&\sum E_{T}^{had} \leq E_{T}^{max}~\chi(r)\nonumber\\
&\mbox{inside each cone with} \;\;      
r^{2}=\left( y - y_{\gamma} \right)^{2} +    
\left(  \phi - \phi_{\gamma} \right)^{2}  \leq R^{2}  \;,    
\end{eqnarray}  
where  the function $\chi(r)$ has to fulfill
\begin{equation}
\chi(r) \rightarrow 0 \;\mbox{if} \; r \rightarrow 0\; \mbox{and}\;
0<\chi(r)< 1 \;\mbox{if} \; 0<r<R\;.
\end{equation}
One possible choice is
\begin{equation}
\label{eq:chi}
\chi(r) = \left( \frac{1-\cos (r)}{1-\cos R} \right)^{n}\;,
\end{equation}
where usually $n=1$ and $E_{T}^{max}=\eps_f\,p_T^\gamma$ are chosen.
%\begin{equation}
%E_{{\rm had, max}} (r_{\gamma}) = \epsilon_f \,p_{T}^{\gamma} \left( \frac{1-\cos r_\gamma}
% {1-\cos R_\gamma}\right)^{n}\;.
% \end{equation}

However, due to finite detector resolution, an 
 experimental realization of this criterion will only be possible to some minimal value of $r$, thereby 
 leaving potentially a residual collinear contribution. 

We would also like to point out that $\epsilon_f$ and $\epsilon_c$ are
not directly comparable, because $\epsilon_f$ is still multiplied by
$\chi(r)$ and therefore can be much larger than $\epsilon_c$ for
comparable values of $\sum E_{T}^{had}$.

%Owens fragmentation functions \cite{Owens:1986mp}.

\subsection{Results}

\subsubsection{Diphotons at NLO}
We compare the standard and the smooth cone~\cite{Frixione:1998hn} isolation prescriptions using an acceptance criterion based on Higgs boson searches and studies~\cite{aad:2012gk}. We present numerical results at NLO requiring $p_T^{\rm harder} \geq 40$~GeV and $p_T^{\rm softer}\geq 30$~GeV, and restricting the rapidity of both photons to $|y_\gamma|<2.37$.  We use the NNPDF23\_nlo\_as\_0119~\cite{Ball:2012cx} PDF set with densities and $\alpha_s$ evaluated at each corresponding order (i.e., we use $(n+1)$-loop $\alpha_s$ at N$^n$LO, with $n=0,1$) and we consider $N_f=5$ massless quarks/antiquarks as well as gluons in the initial state. The QED coupling constant $\alpha$ is fixed to $\alpha=1/137$. We show results for three different values for the centre of mass energy\,: $\sqrt{s} = 8$~TeV, $14$~TeV and $100$~TeV, where we consider the standard cone isolation criterion implemented in the numerical code {\tt DiPhox}~\cite{Binoth:1999qq}, and we use two different sets of fragmentation functions: the Bourhis-Fontannaz-Guillet (BFG)~\cite{Bourhis:1997yu} set\footnote{The BFG set is obtained from an NLO fit to LEP data.} and the LO fragmentation functions by Owens\footnote{The LO Owens fragmentation functions are used only in combination with the LO matrix elements.}~\cite{Owens:1986mp}. The smooth cone isolation prescription is implemented in the numerical code $2\gamma${\tt NNLO}~\cite{Catani:2011qz}. We compare both isolation criteria using the following isolation parameters: $\epsilon_c=\epsilon_f=0.05$,~$0.1$, and $0.5$. We impose a size of the cone $R=0.4$ and in the smooth cone isolation criterion we use $n=1$.


\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{Mgg_01_all_smooth_BFG.pdf}
\hfill
\includegraphics[width=0.47\textwidth]{Mgg_05_all_smooth_BFG.pdf}
\caption{Comparison of different isolation criteria at $\sqrt{s}=8$~TeV, 14~TeV and 100~TeV. In the left panel we show the invariant mass distribution obtained with $\epsilon = 0.1$ and in the right panel the corresponding one for $\epsilon = 0.5$ \label{fig:prelim}.}
\end{figure}

In Fig.~\ref{fig:prelim} we show the invariant mass distributions at the centre of mass energies $\sqrt{s}=8$~TeV, 14~TeV and 100~TeV for two values of the isolation parameter: $\epsilon = 0.1$ (left panel) and $\epsilon = 0.5$ (right panel). In Fig.~\ref{fig:prelim} we notice the different orders of magnitude of the cross-sections depending on the centre of mass energy. In the following we study in detail each centre of mass energy and whether it is possible to define a ``\textit{tight isolation prescription}''~\cite{Andersen:2014efa}.

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{8TeV_Mgg_O_BFG_vs_smooth.pdf}
\hfill
\includegraphics[width=0.47\textwidth]{delta_phi_8TeV_BFG_smooth.pdf}
\caption{Comparison of different isolation criteria at $\sqrt{s}=8$~TeV. In the left panel we show the invariant mass distribution and in the right panel the $\Delta \Phi_{\gamma\gamma}$ \label{fig:8TeVDphiMgg}.}
\end{figure}

In the left panel of Fig.~\ref{fig:8TeVDphiMgg} we show the invariant mass distribution of the diphoton pair at NLO for a centre of mass energy of $\sqrt{s}=8$~TeV. For tight isolation parameters ($\epsilon \leq 0.1$) we observe that the two criteria give essentially the same result (agreement at  percent level) considering the BFG fragmentation functions. While for \textit{loose} isolation parameters (in particular for $\epsilon = 0.5$) the result with the smooth cone is about $9\%$ smaller than the standard cone result (with BFG). The cross-section obtained with the Owens set of fragmentation functions coincides (within $1\%$) with the standard cone (BFG) and smooth cone differential cross-sections for the tight isolation parameter $\epsilon=0.05$. 
However, for $\epsilon=0.1$, the cross section obtained with the Owens  fragmentation functions starts to deviate (being about $2.3\%$ larger) from the result with the BFG fragmentation functions and the cross section obtained with the smooth cone isolation criterion. For $\epsilon=0.5$ the Owens result exhibits large deviations from the BFG ($10\%$) and smooth ($19\%$) cross-sections.

In the right panel of Fig.~\ref{fig:8TeVDphiMgg} we present the results for the $\Delta \Phi_{\gamma\gamma}$ distribution. In this differential distribution, $\Delta \Phi_{\gamma\gamma}=\pi$ is the Born-like kinematical region in which the two photons are back-to-back. All the other kinematical regions are away from the back-to-back configuration. Kinematical regions with $\Delta \Phi_{\gamma\gamma}\neq \pi$ receive contributions from NLO real radiation configurations to the direct photon matrix element, or from the fragmentation part. The standard cone cross-section obtained with the BFG set of fragmentation functions is about $4\%$ larger than the smooth cone cross-section ($\Delta \Phi_{\gamma\gamma}\neq \pi$) if tight isolation parameters are considered ($\epsilon \leq 0.1$).
% For $\epsilon = 0.1$ the BFG result is a $10\%$ larger than the smooth result.

For $\epsilon=0.5$, the distribution obtained with the smooth cone isolation is about $37\%$ smaller than the standard cone result with BFG fragmentation functions for $\Delta \Phi_{\gamma\gamma}\neq \pi$. 

The ratio between the smooth cone results with $\epsilon=0.05$ and $\epsilon=0.5$ is close to 1 (within $0.1\%$) for all the kinematical ranges with the exception of the last bin\,\footnote{Concerning the $\Delta \Phi_{\gamma\gamma}$ distribution in the last bin $\Delta \Phi_{\gamma\gamma}\simeq \pi$ the cross section with the smooth cone criterion using $\epsilon=0.5$ is about $12.8\%$ larger than the cross section obtained with $\epsilon=0.05$.} $\Delta \Phi_{\gamma\gamma}\simeq \pi$, which contains the Born-like contributions. This fact can be explained noticing that all  configurations where the extra QCD parton (which is present at NLO 
in the ral radiation part) lies outside the cone around each photon are independent of the value for $\epsilon$ or $E_{T}^{max}$, while the nearly collinear configurations 
are suppressed with smooth isolation, and the ones where the extra parton is soft or collinear to one of the photons contribute to the back-to-back situation 
$\Delta \Phi_{\gamma\gamma} \simeq \pi$.


\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{14TeV_Mgg_O_BFG_vs_smooth.pdf}
\hfill
\includegraphics[width=0.47\textwidth]{delta_phi_14TeV_BFG_smooth.pdf}
\caption{Comparison of different isolation criteria at $\sqrt{s}=14$~TeV. In the left panel we show the invariant mass distribution and in the right panel the $\Delta \Phi_{\gamma\gamma}$ distribution.\label{fig:14TeVDphiMgg}}
\end{figure}

At $\sqrt{s}=14$~TeV (Fig.~\ref{fig:14TeVDphiMgg}), concerning the invariant mass and the $\Delta \Phi_{\gamma\gamma}$ distributions, we have essentially the same considerations as at $\sqrt{s}=8$~TeV. 

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{100TeV_Mgg_BFG_smooth.pdf}
\hfill
\includegraphics[width=0.47\textwidth]{deltaPhi_100TeV_BFG_smooth.pdf}
\caption{Comparison of different isolation criteria at $\sqrt{s}=100$~TeV. In the left panel we show the invariant mass distribution and in the right panel the $\Delta \Phi_{\gamma\gamma}$ \label{fig:100TeVDphiMgg}.}
\end{figure}

In Fig~\ref{fig:100TeVDphiMgg} we present our results for $\sqrt{s}=100$~TeV. In the left panel we show the invariant mass distribution in which we compare the results using the smooth and standard cone isolation criteria. The cross section obtained using the smooth isolation criterion is about $7.5\%$ smaller than the standard cone result using BFG fragmentation functions for $\epsilon=0.05$. For $\epsilon=0.5$, the smooth cone result is about $8.4\%$ smaller than the standard cone result with BFG fragmentation. The cross section obtained using the Owens set of fragmentation functions is about $18\%$ larger than the BFG result with $\epsilon=0.5$, which is almost twice the same ratio at $8$~TeV. Regarding the smooth cone result with $\epsilon=0.5$, the cross section obtained with the Owens set is about $26.4\%$ larger.

In the right panel of Fig~\ref{fig:100TeVDphiMgg} we show the $\Delta \Phi_{\gamma\gamma}$ distribution for $\sqrt{s}=100$~TeV. The cross sections obtained with the standard cone isolation criterion with BFG and Owens fragmentation functions coincide (within $1\%$) if $\epsilon=0.05$. The standard result (with BFG) obtained using $\epsilon=0.1$ is about $23\%$ larger than the cross section with the smooth cone isolation (if $\Delta \Phi_{\gamma\gamma}\neq \pi$). 

Similar to the centre of mass energies already discussed, far away from $\Delta \Phi_{\gamma\gamma}\simeq \pi$ there is no difference between the smooth cone result with $\epsilon=0.05$ and $\epsilon=0.5$.

At $\sqrt{s}=100$~TeV  the fragmentation functions, extracted at lower energies (from LEP data), reach the limit of their validity range. Notice that the invariant mass distributions (left panel of Fig~\ref{fig:100TeVDphiMgg}), obtained with the standard cone isolation criterion, show a rising slope in the last bins, which most likely comes from the fact that the range of validity of the fragmentation functions (BFG quote a range of 10\,GeV $\leq Q\leq$ 100\,TeV) is surpassed.

At $\sqrt{s}=8$~TeV and 14~TeV it is possible to define a tight isolation prescription as done in the 2013 Les Houches proceedings~\cite{Andersen:2014efa}.

If we require $\epsilon\leq0.05$ the two isolation criteria give very similar cross sections, with agreement at the percent level, also at the level of  distributions. For $0.05 \leq \epsilon \leq 0.1$ we have agreement at the percent level, for all the kinematical regions in which a LO Born cross-section is present. For kinematical regions far way from the back-to-back configuration, in which the direct LO contribution vanishes (i.e $\Delta \Phi_{\gamma\gamma}\neq \pi$ in the $\Delta \Phi_{\gamma\gamma}$ distribution), the discrepancies between cross-sections obtained using the standard and the smooth cone isolation criteria are at the $4\%$ percent level. Given the size of the scale uncertainties at these energies and at this perturbative order ($\pm 6\%$ percent at the level of total cross-sections), a tight isolation prescription ($\epsilon < 0.1$) allows the comparison of cross sections obtained using these two different types of isolation criteria.

\subsubsection{Photon + Jet at NLO}

In this section we use the program {\tt
  JetPhox}~\cite{Aurenche:2006vj,Belghobsi:2009hx} to calculate cross
sections for the production of a photon and a jet. 
As {\tt JetPhox} allows to calculate the fragmentation component at
NLO, it can be used to assess the importance of this contribution 
for various isolation parameters and centre of mass energies.

Unless stated otherwise, we use the Bourhis-Fontannaz-Guillet (BFG)~\cite{Bourhis:1997yu}
fragmentation functions (set II).
For comparison, we also give results obtained with the (LO) fragmentation functions by Owens~\cite{Owens:1986mp},
which we use for consistency only in combination with the leading order matrix elements.

We use the following  settings and cuts: the NNPDF23\_nlo\_as\_0119~\cite{Ball:2012cx} 
PDF set with $\alpha_s$
taken from the PDFs.
For the renormalisation, factorisation and fragmentation scales we use
 $\mu=\mu_f=\mu_F=p_T^\gamma$.  The  photon is required to be in the rapidity range 
 $ |y^\gamma|\leq 2.37$ and to have a minimum transverse momentum of $p_{T,min}^\gamma=30$\,GeV.
For the jet we use the $k_T$-algorithm with $R=0.4$ and the rapidity and transverse momentum cuts 
$|y^{jet}|\leq 4.7$, $p_{T,min}^{jet}=25$\,GeV.

We will compare three different isolation prescriptions:
\begin{itemize}
\item[(a)] smooth isolation (``Frixione isolation''~\cite{Frixione:1998hn}) 
as defined in eq.~(\ref{eq:frixisol}), with $R=0.4$, $n=1$ and various values of $\eps_f$,
\item[(b)] standard cone isolation as defined in eq.~(\ref{eq:coneisol})
with $R=0.4$ and $E_{T}^{max}= \eps_c\, p_{T}^{\gamma}$,
\item[(c)] ``hybrid'' cone isolation with $E_{T}^{max}$ composed of both, 
a fixed amount of energy and a fraction of the photon transverse momentum, as used e.g. 
in Ref.~\cite{ATLAS-CONF-2015-081}: 
$E_{T}^{max}= \eps\, p_{T}^{\gamma}+ $6\,GeV with $\eps=0.05$.
\end{itemize}
In Table~\ref{tab:frix} we show results for the total NLO cross sections using criterion (a) 
at $\sqrt{s}=8, 14$ and 100 TeV.
Table \ref{tab:cone} shows results for the standard cone isolation criterion (b), 
Table \ref{tab:hybrid} for the hybrid cone isolation criterion (c). 
The results for the direct and fragmentation parts are shown separately. 
In particular, one can see that the NLO corrections to the fragmentation part are substantial, 
in particular for small values of $\epsilon_c$, with K-factors (for the fragmentation component) 
of about 2.2 (8 TeV) to 2.4 (100 TeV) 
for standard cone isolation, and about 1.7 for hybrid cone isolation. With the hybrid isolation criterion, 
this K-factor does not increase as the centre of mass energy increases. However, as the fragmentation 
component is about one order of magnoitude smaller than the direct component, 
the K-factor for the fragmentation part does not play a major role.

The results for the total cross sections at $\sqrt{s}=8\,$TeV obtained with the 
three criteria are compared to each other in Fig.~\ref{fig:comp_conefrix}. 
As to be expected, the hybrid isolation leads to larger values of the cross section 
than the standard cone isolation for the same value of $\epsilon_c$, because it allows 
a larger fragmentation component.
It is interesting to observe that, in contrast to the diphoton case, the cross sections 
obtained with the smooth isolation criterion are always larger than the ones obtained with 
standard or hybrid cone isolation. This means that the subtraction terms for collinear configurations 
in the direct photon component  are more dominant in the photon plus jet case than in the diphoton case.
 

Fig.~\ref{fig:compfrag} shows various options for the fragmentation contribution separately, 
for $\sqrt{s}=14\,$TeV.
In Figs.~\ref{fig:pTisohybrid}, \ref{fig:deltaRisohybrid}, \ref{fig:deltaPhiisohybrid} 
we compare the standard and the hybrid cone isolation for the distributions of 
$p_T^\gamma$, $\Delta R^{\gamma-jet}$ and $\Delta \phi^{\gamma-jet}$, respectively.  


\begin{table}
\centering
\begin{tabular}{|c|l|r|r|r|}
\hline
$\sqrt{s}$ [TeV]&smooth isol.&$\eps_f=0.05$&$\eps_f=0.1$&$\eps_f=0.5$\\
\hline
8 &LO& 13.172&13.172&13.172\\
   &NLO& 25.418& 25.757& 27.501\\
\hline
14&LO&23.396&23.396&23.396\\
    &NLO& 46.793&  47.409&50.719 \\
\hline
100& LO& 13.607& 13.607& 13.607\\
&NLO& 300.70& 305.09&327.76\\
\hline
\end{tabular}
\caption{Total cross sections (in nanobarn) for the smooth isolation criterion~\cite{Frixione:1998hn} with
  different values of $\eps_f$.\label{tab:frix}}
\end{table}

\begin{table}
\centering
\begin{tabular}{|c|l|r|r|r|}
\hline
$\sqrt{s}$ [TeV]&standard cone isol.&$\eps_c=0.05$&$\eps_c=0.1$&$\eps_c=0.25$\\
\hline
8 &dir NLO& 22.682&21.249&18.803\\
   &frag LO (BFG)&0.824&1.738& 3.956\\
  & frag LO (Owens)& 0.930&1.980&4.808\\
  & frag NLO   &2.017&3.539&6.636\\
 & total NLO   &24.699&24.788& 25.439\\
%& dir NLO+frag ME LO& &22987.63&22759.44\\
%& dir NLO + frag ME and FF LO&&23229.56&23612.24\\
\hline
14&dir NLO&41.577&38.861&34.145 \\
   &frag LO (BFG)&1.559&3.296&7.554 \\
   & frag LO (Owens)& 1.772& 3.788& 9.296\\
  & frag NLO  &3.815&6.682&12.577\\
 & total NLO&45.393&45.544&46.724\\
\hline
100&dir NLO&265.44&246.55&213.66 \\
   &frag LO  &10.593 &22.547&52.634\\
%   & frag LO (Owens)& 
  & frag NLO&25.688&45.321&86.586\\
 & total NLO&291.13&291.87&300.25\\
\hline
\end{tabular}
\caption{Total cross sections (in nanobarn) for the cone isolation
  criterion with $R=0.4$ and
  different values of $\eps_c$.
  Note that ``frag LO'' refers to the matrix element being calculated at leading order, 
  while the BFG fragmentation functions are always NLO fits, the Owens fragmentation functions LO fits.\label{tab:cone}}
\end{table}

\begin{table}
\centering
\begin{tabular}{|c|l|r|r|r|}
\hline
$\sqrt{s}$ [TeV]&hybrid cone isol.&$\eps_c=0.05$&$\eps_c=0.1$&$\eps_c=0.25$\\
\hline
8 &dir NLO&19.392&18.769&17.504\\
   &frag LO&3.324&3.987&5.625\\
  & frag NLO&5.744&6.646&8.751\\
 & total NLO&25.136&25.416&26.255\\
\hline
14&dir NLO&35.302&34.111&31.631\\
   &frag LO&6.288&7.572&10.775\\
  & frag NLO&10.797&12.527&16.627\\
 & total NLO&46.099&46.638&48.259\\
\hline
100&dir NLO&223.201&214.327&195.901\\
   &frag LO&42.409&51.708&75.429\\
  & frag NLO&72.411&84.668&114.829\\
 & total NLO&295.61&298.99&310.73\\
\hline
\end{tabular}
\caption{Total cross sections (in nanobarn) for the cone isolation
  criterion which uses a ``hybrid'' isolation~\cite{ATLAS-CONF-2015-081} considering both fixed energy in the cone and a
  fraction of the photon transverse momentum, with $R=0.4$,
  $E_{T,fix}^{cone}=6$\,GeV  and
  different values of $\eps_c$.\label{tab:hybrid}}
\end{table}

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{rs8_comp_conefrix.pdf}
\hfill
\includegraphics[width=0.47\textwidth]{rs14_comp_conefrix.pdf}
\caption{Comparison of different isolation criteria at $\sqrt{s}=8$ and 14\,TeV.\label{fig:comp_conefrix}}
\end{figure}

\begin{figure}[t!]
\includegraphics[width=0.47\textwidth]{rs8_compfrag.pdf}
\hfill
\includegraphics[width=0.47\textwidth]{rs14_compfrag.pdf}
\caption{Comparison of different fragmentation components at $\sqrt{s}=8$ and 14\,TeV.\label{fig:compfrag}}
\end{figure}

\begin{figure}[t!]
\centering
\includegraphics[width=0.95\textwidth]{pTgamma_iso_hybrid.pdf}
\caption{Photon transverse momentum distribution for standard and hybrid cone isolation at (a) $\sqrt{s}=8$~TeV, 
(b) $\sqrt{s}=14$~TeV and (c) $\sqrt{s}=100$~TeV. 
Panel (d) compares different fragmentation contributions at $\sqrt{s}=14$~TeV, where the 
isolation parameters are the same as in the other panels.\label{fig:pTisohybrid}}
\end{figure}

\begin{figure}[t!]
\centering
\includegraphics[width=0.95\textwidth]{DeltaR_compare_iso.pdf}
\caption{Photon-jet $R$-separation  for standard and hybrid cone isolation at $\sqrt{s}=8$ TeV. \label{fig:deltaRisohybrid}}
\end{figure}

\begin{figure}[t!]
\centering
\includegraphics[width=0.95\textwidth]{DeltaPhi_compare_iso.pdf}
\caption{Photon-jet azimuthal angle separation  for standard and hybrid cone isolation at $\sqrt{s}=8$ TeV. \label{fig:deltaPhiisohybrid}}
\end{figure}

\clearpage

\subsection{Summary}
We have studied various photon isolation criteria at $\sqrt{s}=8,14$ and 100 TeV for both diphoton production and photon plus jet production at NLO. 
In the diphoton case, we observe that, 
for tight isolation parameters ($\epsilon \leq 0.1$),  the smooth and standard cone isolation criteria give  results which agree  at the  percent level, 
where the standard cone isolation always gives a larger result. 
For looser isolation, the two criteria start to deviate, the differences being in the 10\% range for $\epsilon = 0.5$ for the $m_{\gamma\gamma}$ distribution, 
and even larger for the $\Delta \Phi_{\gamma\gamma}$ distribution.

At 100 TeV, the fragmentation functions may be called at  scales which are close to their limit of validity. 
Therefore a new fit of the photon fragmentation functions based on LHC data would be desirable.

The photon+jet case shows a different behaviour when comparing the smooth isolation with  cone isolation including the fragmentation part, 
in the sense that the smooth cone result is larger than the ``standard'' cone result, which is the opposite behaviour as in the diphoton case.
However, for tight isolation, the results with the two criteria also agree within a few percent.\\
In addition, a new isolation criterion, used recently by {\sc Atlas}~\cite{ATLAS-CONF-2015-081} has been implemented, 
where both a fixed energy in the cone and a fraction of the photon transverse momentum are used to determine the maximally allowed hadronic energy in the cone. 
This ``hybrid'' criterion leads to results which are very similar to the ``standard'' cone isolation where only a fraction of the photon momentum is used, 
except for low $p_T^\gamma$ values, where the spectrum with hybrid isolation is slightly shifted towards larger  $p_T^\gamma$ values.

