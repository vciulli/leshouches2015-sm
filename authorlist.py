#!/usr/bin/python

import os
import numpy as np
import sys
import re
from collections import OrderedDict

class affiliation:

    def __init__(self, institution = ['']):
        self.convenor = 0
        self.institution = institution
        self.instindex = [1]

def main():

  Names = {}
  Names['Andersen']          = 'J.~R.~Andersen'
  Names['Badger']            = 'S.~Badger'
  Names['Becker']            = 'K.~Becker'
  Names['Bellm']             = 'J.~Bellm'
  Names['Boughezal']         = 'R.~Boughezal'
  Names['Falmagne']          = 'G.~Falmagne'
  Names['Frederix']          = 'R.~Frederix'
  Names['Grazzini']          = 'M.~Grazzini'
  Names['Greiner']           = 'N.~Greiner'
  Names['Hoeche']            = 'S.~H{\\\"o}che'
  Names['Huston']            = 'J.~Huston'
  Names['Isaacson']          = 'J.~Isaacson'
  Names['Li']                = 'Y.~Li'
  Names['Liu']               = 'X.~Liu'
  Names['Liusoni']           = 'G.~Luisoni'
  Names['Petriello']         = 'F.~Petriello'
  Names['Platzer']           = 'S.~Pl{\\\"a}tzer'
  Names['Prestel']           = 'S.~Prestel'
  Names['Pogrenyak']         = 'I.~Pogrebnyak'
  Names['Schichtel']         = 'P.~Schichtel'
  Names['Schoenherr']        = 'M.~Sch{\\\"o}nherr'
  Names['Sun']               = 'P.~Sun'
  Names['TackmannF']         = 'F.~J.~Tackmann'
  Names['Vryonidou']         = 'E.~Vryonidou'
  Names['Winter']            = 'J.~Winter'
  Names['YuanCP']            = 'C.-P.~Yuan'
  Names['YuanF']             = 'F.~Yuan'
  Names['Chiesa']            = 'M.~Chiesa'
  Names['Ciulli']            = 'V.~Ciulli'
  Names['Cieri']             = 'L.~Cieri'
  Names['Denner']            = 'A.~Denner'
  Names['Hofer']             = 'L.~Hofer'
  Names['Kallweit']          = 'S.~Kallweit'
  Names['Lindert']           = 'J.M.~Lindert'
  Names['Maierhofer']        = 'P.~Maierh{\\\"o}fer'
  Names['Marini']            = 'A.~C.~Marini'
  Names['Montagna']          = 'G.~Montagna'
  Names['Moretti']           = 'M.~Moretti'
  Names['Nicrosini']         = 'O.~Nicrosini'
  Names['Pagani']            = 'D.~Pagani'
  Names['Piccinini']         = 'F.~Piccinini'
  Names['Pozzorini']         = 'S.~Pozzorini'
  Names['Takasugi']          = 'E.~Takasugi'
  Names['Uccirati']          = 'S.~Uccirati'
  Names['Weber']             = 'M.~A.~Weber'
  Names['Zaro']              = 'M.~Zaro'
  Names['Williams']          = 'C.~Williams'
  Names['Heinrich']          = 'G.~Heinrich'
  Names['Maitre']            = 'D.~Ma{\\^i}tre'
  Names['Gao']               = 'J.~Gao'
  Names['Hou']               = 'T.-J.~Hou'
  Names['Nadolsky']          = 'P.~M.~Nadolsky'
  Names['Wang']              = 'B.~T.~Wang'
  Names['Xie']               = 'K.~P.~Xie'
  Names['Carrazza']          = 'S.~Carrazza'
  Names['Huss']              = 'A.~Huss'
  Names['Forte']             = 'S.~Forte'
  Names['Kassabov']          = 'Z.~Kassabov'
  Names['Rojo']              = 'J.~Rojo'
  Names['Bendavid']          = 'J.~Bendavid'
  Names['TackmannK']         = 'K.~Tackmann'
  Names['Lazopoulos']        = 'A.~Lazapoulos'
  Names['Kuttimalai']        = 'S.~Kuttimalai'
  Names['Krauss']            = 'F.~Krauss'
  Names['Duehrssen-Debling'] = 'M.~Duehrssen-Debling'
  Names['Francavilla']       = 'P.~Francavilla'
  Names['Soyez']             = 'G.~Soyez'
  Names['Thaler']            = 'J.~Thaler'
  Names['Bein']              = 'S.~Bein'
  Names['Buckley']           = 'A.~Buckley'
  Names['Butterworth']       = 'J.~Butterworth'
  Names['Campanelli']        = 'M.~Campanelli'
  Names['Larkoski']          = 'A.~Larkoski'
  Names['Loch']              = 'P.~Loch'
  Names['Nachman']           = 'B.~Nachman'
  Names['Nagy']              = 'Z.~Nagy'
  Names['Pollard']           = 'C.~Pollard'
  Names['Rappoccio']         = 'S.~Rappoccio'
  Names['Salam']             = 'G.~Salam'
  Names['Schmidt']           = 'A.~Schmidt'
  Names['Waalewijn']         = 'W.~Waalewijn'
  Names['Freytsis']          = 'M.~Freytsis'
  Names['Gras']              = 'P.~Gras'
  Names['Kar']               = 'D.~Kar'
  Names['Lonnblad']          = 'L.~L{\\\"o}nnblad'
  Names['Siodmok']           = 'A.~Si{\\\'o}dmok'
  Names['Skands']            = 'P.~Skands'
  Names['Soper']             = 'D.~Soper'
  Names['Bell']              = 'M.~Bell'
  Names['Hesketh']           = 'G.~Hesketh'
  Names['Nail']              = 'G.~Nail'
  Names['Napoletano']        = 'D.~Napoletano'
  Names['Oleari']            = 'C.~Oleari'
  Names['Perrozzi']          = 'L.~Perrozzi'
  Names['Reuschle']          = 'C.~Reuschle'
  Names['Waugh']             = 'B.~Waugh'
  Names['Francavilla']       = 'P.~Francavilla'
  Names['Konstantinides']    = 'V.~Konstantinides'
  Names['Lenzi']             = 'P.~Lenzi'
  Names['Pandini']           = 'C.~Pandini'
  Names['Russo']             = 'L.~Russo'
  Names['Utku']              = 'U.~Utku'
  Names['Viliani']           = 'L.~Viliani'
  Names['Bothmann']          = 'E.~Bothmann'
  Names['Schumann']          = 'S.~Schumann'
  Names['Jueid']             = 'A.~Jueid'
  Names['Zapp']              = 'K.~Zapp'

  Address = {}
  Address['Edinburgh']     = 'Higgs Centre for Theoretical Physics, School of Physics and Astronomy, The University of Edinburgh, Edinburgh EH9 3JZ, Scotland, UK'
  Address['IPPP']          = 'Institute for Particle Physics Phenomenology, University of Durham, Durham DH1 3LE, UK'
  Address['MSU']           = 'Department of Physics and Astronomy, Michigan State University, East Lansing, MI 48824, USA'
  Address['CERN']          = 'CERN, TH Department, CH--1211 Geneva, Switzerland'
  Address['CERNEP']        = 'CERN, EP Department, CH--1211 Geneva, Switzerland'
  Address['SLAC']          = 'SLAC National Accelerator Laboratory, Menlo Park, CA 94025, USA'
  Address['UZH']           = r'Physik--Institut, Universit\"at Z\"urich, Wintherturerstrasse 190, CH--8057 Z\"urich, Switzerland'
  Address['ETHITP']        = r'Institute for Theoretical Physics, ETH, CH--8093 Z\"urich, Switzerland'
  Address['ETHIPP']        = r'Institute for Particle Physics, ETH, CH--8093 Z\"urich, Switzerland'
  Address['Wurzburg']      = r'Universit\"{a}t W\"{u}rzburg, Institut f\"{u}r Theoretische Physik und Astrophysik, D-97074 W\"{u}rz\-burg, Germany'
  Address['Berkeley']      = 'Nuclear Science Division, Lawrence Berkeley National Laboratory, Berkeley, CA 94720, USA'
  Address['Louvain']       = r'Centre for Cosmology, Particle Physics and Phenomenology (CP3), Universit\'e catholique de Louvain, B-1348 Louvain-la-Neuve, Belgium'
  Address['Fermilab']      = 'Fermilab, PO Box 500, Batavia, IL 60510, USA'
  Address['Argonne']       = 'High Energy Physics Division, Argonne National Laboratory, Argonne, IL 60439, USA'
  Address['Northwestern']  = 'Department of Physics \& Astronomy, Northwestern University, Evanston, IL 60208, USA'
  Address['ENSCachan']     = r'ENS Cachan, Universit\'e Paris--Saclay, 61 avenue du Pr\'esident Wilson, Cachan, F--94230, France'
  Address['Maryland']      = 'Maryland Center for Fundamental Physics, University of Maryland, College Park, Maryland 20742, USA'
  Address['MPIMunich']     = r'Max Planck Institute for Physics, F\"ohringer Ring 6, 80805 M\"unchen, Germany'
  Address['DallasSM']      = 'Department of Physics, Southern Methodist University, Dallas, TX 75275-0181, USA'
  Address['Milan']         = r'TIF Lab, Dipartimento di Fisica, Universit\`a di Milano and INFN, Sezione di Milano, Via Celoria 16, I-20133 Milano, Italy'
  Address['Torino']        = 'Dipartimento di Fisica, Universit\`a di Torino and INFN, Sezione di Torino, Via Pietro Giuria 1, I-10125 Torino, Italy'
  Address['Oxford']        = 'Rudolf Peierls Centre for Theoretical Physics, 1 Keble Road,University of Oxford, OX1 3NP Oxford, United Kingdom'
  Address['Tanger']        = r'D\'epartement des Math\'ematiques, Universit\'e AbdelMalek Essaadi, Tanger, Morocco'
  Address['SISSA']         = 'The Abdus Salam International Centre for Theoretical Physics, Trieste, Italy'
  Address['Manchester']    = 'Particle Physics Group, School of Physics and Astronomy, University of Manchester, Manchester M13 9PL, UK'
  Address['DESY']          = 'Deutsches Elektronen-Synchrotron (DESY), D-22607 Hamburg, Germany'
  Address['Krakow']        = r'Institute of Nuclear Physics, Polish Academy of Sciences, ul. Radzikowskiego 152, 31-342 Krak\'ow, Poland'
  Address['OxfordEP']      = 'Department of Physics, Oxford University, Denys Wilkinson Building, Keble Road, Oxford OX1 3RH, UK'
  Address['TUM']           = r'Physik Department T31, Technische Universit\"at M\"unchen, 85748 Garching, Germany'
  Address['Monash']        = 'School of Physics and Astronomy, Monash University, VIC-3800, Australia'
  Address['Oregon']        = 'Institute of Theoretical Science, University of Oregon, Eugene, OR 97403-5203, USA'
  Address['Gottingen']     = r'II. Physikalisches Institut, Universit\"at G\"ottingen, 37077 G\"ottingen, Germany'
  Address['Buffalo']       = 'Department of Physics, University at Buffalo, The State University of New York, Buffalo 14260, USA'
  Address['UCL']           = 'Department of Physics and Astronomy, University College London, Gower Street, London WC1E 6BT, UK'
  Address['Pavia']         = 'INFN, Sezione di Pavia, Via A. Bassi 6, 27100 Pavia, Italy'
  Address['PaviaUni']      = r'Dipartimento di Fisica, Universit\`a di Pavia'
  Address['FlorenceINFN']  = r'INFN, Sezione di Firenze, Firenze, Italy'
  Address['Florence']      = r'Dipartimento di Fisica e Astronomia, Universit\`a di Firenze, I-50019 Sesto Fiorentino, Florence, Italy'
  Address['Barcelona']     = r'Department de F\'isica Qu\`antica i Astrof\'isica (FQA), Institut de Ci\`encies del Cosmos (ICCUB), Universitat de Barcelona (UB), Mart\'i Franqu\`es 1, E-08028 Barcelona, Spain'
  Address['Mainz']         = 'PRISMA Cluster of Excellence, Institute of Physics, Johannes Gutenberg University, D-55099 Mainz, Germany'
  Address['Freiburg']      = r'Physikalisches Institut, Albert-Ludwigs-Universit\"{a}t Freiburg, 79104 Freiburg, Germany'
  Address['Ferrara']       = r'Dipartimento di Fisica, Universit\`a di Ferrara and INFN, Sezione di Ferrara, Italy'
  Address['LPTHE']         = r'Sorbonne Universit\'{e}s, UPMC Univ. Paris 06, UMR 7589, LPTHE, F-75005, Paris, France'
  Address['LPTHECNRS']     = r'CNRS, UMR 7589, LPTHE, F-75005, Paris, France'
  Address['ParisPMC']      = r'Laboratoire de Physique Nucl\'{e}aire et de Hautes Energies, UPMC and Universit\'{e} Paris-Diderot and CNRS/IN2P3, Paris, France'
  Address['Lund']          = 'Dept. of Astronomy and Theoretical Physics, Lund University, Sweden'
  Address['Amsterdam']     = 'ITFA, University of Amsterdam, Science Park 904, 1018 XE, Amsterdam, The Netherlands'
  Address['Nikhef']        = 'Nikhef, Theory Group, Science Park 105, 1098 XG, Amsterdam, The Netherlands'
  Address['MilanB']        = r'Universit\`a di Milano-Bicocca, Milano, Italy'
  Address['MilanBINFN']    = 'INFN, Sezione di Milano-Bicocca, Piazza della Scienza 3, 20126 Milano, Italy'
  Address['IPhT']          = 'IPhT, CEA Saclay, CNRS UMR 3681, F-91191 Gif-sur-Yvette, France'
  Address['MIT']           = 'Center for Theoretical Physics, Massachusetts Institute of Technology, Cambridge, MA 02139, USA'
  Address['KarlsruheITP']  = r'Institut f\"{u}r Theoretische Physik, Karlsruhe Institute of Technology, 76131 Karlsruhe, Germany'
  Address['Florida']       = 'HEP Theory Group, Department of Physics, Florida State University, Tallahassee, USA'
  Address['SaclayCEA']     = 'CEA/IRFU Saclay, France'
  Address['Harvard']       = 'Center for the Fundamental Laws of Nature, Harvard University, Cambridge, MA 02138'
  Address['Tucson']        = 'Department of Physics, University of Arizona, Tucson AZ, USA'
  Address['Glasgow']       = 'School of Physics and Astronomy, University of Glasgow, Glasgow, United Kingdom'
  Address['UCLA']          = 'University of California, Los Angeles, USA'
  Address['Hamburg']       = 'University of Hamburg, Hamburg, Germany'
  Address['Witwatersrand'] = 'School of Physics, University of the Witwatersrand, Johannesburg, Wits 2050, South Africa'
  Address['Caltech']       = 'California Institute of Technology, Pasadena, USA'
  Address['LPHNE']         = r'Laboratoire de Physique Nucl\'{e}aire et de Hautes Energies, Institut Lagrange de Paris, CNRS, Paris, France'
  Address['Sienna']        = r'Universit\`{a} di Siena, Siena, Italy'

  Authors = {};
  Authors['Andersen']          = affiliation(['IPPP'])
  Authors['Badger']            = affiliation(['Edinburgh'])
  Authors['Becker']            = affiliation(['OxfordEP'])
  Authors['Bellm']             = affiliation(['IPPP'])
  Authors['Boughezal']         = affiliation(['Argonne'])
  Authors['Falmagne']          = affiliation(['ENSCachan'])
  Authors['Frederix']          = affiliation(['TUM'])
  Authors['Grazzini']          = affiliation(['UZH'])
  Authors['Greiner']           = affiliation(['UZH'])
  Authors['Hoeche']            = affiliation(['SLAC'])
  Authors['Huston']            = affiliation(['MSU'])
  Authors['Isaacson']          = affiliation(['MSU'])
  Authors['Li']                = affiliation(['Fermilab'])
  Authors['Liu']               = affiliation(['Maryland'])
  Authors['Liusoni']           = affiliation(['CERN'])
  Authors['Petriello']         = affiliation(['Argonne','Northwestern'])
  Authors['Platzer']           = affiliation(['IPPP', 'Manchester'])
  Authors['Prestel']           = affiliation(['SLAC'])
  Authors['Pogrenyak']         = affiliation(['MSU'])
  Authors['Schichtel']         = affiliation(['IPPP'])
  Authors['Schoenherr']        = affiliation(['UZH'])
  Authors['Sun']               = affiliation(['MSU'])
  Authors['TackmannF']         = affiliation(['DESY'])
  Authors['Vryonidou']         = affiliation(['Louvain'])
  Authors['Winter']            = affiliation(['MSU'])
  Authors['YuanCP']            = affiliation(['MSU'])
  Authors['YuanF']             = affiliation(['Berkeley'])
  Authors['Huss']              = affiliation(['ETHITP'])
  Authors['Chiesa']            = affiliation(['Pavia'])
  Authors['Ciulli']            = affiliation(['Florence','FlorenceINFN'])
  Authors['Cieri']             = affiliation(['UZH'])
  Authors['Denner']            = affiliation(['Wurzburg'])
  Authors['Hofer']             = affiliation(['Barcelona'])
  Authors['Kallweit']          = affiliation(['Mainz'])
  Authors['Lindert']           = affiliation(['UZH'])
  Authors['Maierhofer']        = affiliation(['Freiburg'])
  Authors['Marini']            = affiliation(['MIT'])
  Authors['Montagna']          = affiliation(['Pavia','PaviaUni'])
  Authors['Moretti']           = affiliation(['Ferrara'])
  Authors['Nicrosini']         = affiliation(['Pavia'])
  Authors['Pagani']            = affiliation(['Louvain'])
  Authors['Piccinini']         = affiliation(['Pavia'])
  Authors['Pozzorini']         = affiliation(['UZH'])
  Authors['Takasugi']          = affiliation(['MPIMunich','UCLA'])
  Authors['Uccirati']          = affiliation(['Torino'])
  Authors['Weber']             = affiliation(['CERN','UCLA'])
  Authors['Zaro']              = affiliation(['LPTHE', 'LPTHECNRS'])
  Authors['Williams']          = affiliation(['Buffalo'])
  Authors['Heinrich']          = affiliation(['MPIMunich'])
  Authors['Maitre']            = affiliation(['IPPP'])
  Authors['Gao']               = affiliation(['Argonne'])
  Authors['Hou']               = affiliation(['DallasSM'])
  Authors['Nadolsky']          = affiliation(['DallasSM'])
  Authors['Wang']              = affiliation(['DallasSM'])
  Authors['Xie']               = affiliation(['MSU'])
  Authors['Carrazza']          = affiliation(['CERN'])
  Authors['Forte']             = affiliation(['Milan'])
  Authors['Kassabov']          = affiliation(['Milan','Torino'])
  Authors['Rojo']              = affiliation(['Oxford'])
  Authors['Bendavid']          = affiliation(['Caltech'])
  Authors['TackmannK']         = affiliation(['DESY'])
  Authors['Lazopoulos']        = affiliation(['ETHITP'])
  Authors['Kuttimalai']        = affiliation(['IPPP'])
  Authors['Krauss']            = affiliation(['IPPP'])
  Authors['Duehrssen-Debling'] = affiliation(['CERNEP'])
  Authors['Francavilla']       = affiliation(['LPNHE'])
  Authors['Soyez']             = affiliation(['IPhT'])
  Authors['Thaler']            = affiliation(['MIT'])
#  Authors['Bein']              = affiliation(['Florida'])
#  Authors['Buckley']           = affiliation(['Glasgow'])
  Authors['Butterworth']       = affiliation(['UCL'])
#  Authors['Campanelli']        = affiliation(['UCL'])
#  Authors['Larkoski']          = affiliation(['Harvard'])
#  Authors['Loch']              = affiliation(['Tucson'])
#  Authors['Nachman']           = affiliation(['SLAC'])
#  Authors['Nagy']              = affiliation(['DESY'])
#  Authors['Pollard']           = affiliation(['Glasgow'])
#  Authors['Rappoccio']         = affiliation(['Buffalo'])
#  Authors['Salam']             = affiliation(['CERN'])
#  Authors['Schmidt']           = affiliation(['Hamburg'])
#  Authors['Waalewijn']         = affiliation(['Amsterdam', 'Nikhef'])
  Authors['Freytsis']          = affiliation(['Oregon'])
  Authors['Gras']              = affiliation(['SaclayCEA'])
  Authors['Kar']               = affiliation(['Witwatersrand'])
  Authors['Lonnblad']          = affiliation(['Lund'])
  Authors['Siodmok']           = affiliation(['CERN'])
  Authors['Skands']            = affiliation(['Monash'])
  Authors['Soper']             = affiliation(['Oregon'])
  Authors['Bell']              = affiliation(['UCL'])
  Authors['Hesketh']           = affiliation(['UCL'])
  Authors['Nail']              = affiliation(['Manchester','KarlsruheITP'])
  Authors['Napoletano']        = affiliation(['IPPP'])
  Authors['Oleari']            = affiliation(['MilanB','MilanBINFN'])
  Authors['Reuschle']          = affiliation(['Florida'])
  Authors['Waugh']             = affiliation(['UCL'])
  Authors['Francavilla']       = affiliation(['LPHNE'])
  Authors['Konstantinides']    = affiliation(['UCL'])
  Authors['Lenzi']             = affiliation(['FlorenceINFN'])
  Authors['Pandini']           = affiliation(['ParisPMC'])
  Authors['Perrozzi']          = affiliation(['ETHIPP'])
  Authors['Russo']             = affiliation(['FlorenceINFN','Sienna'])
  Authors['Utku']              = affiliation(['UCL'])
  Authors['Viliani']           = affiliation(['Florence', 'FlorenceINFN', 'CERNEP'])
  Authors['Bothmann']          = affiliation(['Gottingen'])
  Authors['Schumann']          = affiliation(['Gottingen'])
  Authors['Jueid']             = affiliation(['Tanger'])
  Authors['Zapp']              = affiliation(['CERN'])

  AuthorList = sorted([name for name in Authors])
  InstituteList = list(OrderedDict.fromkeys(sum([Authors[name].institution for name in AuthorList],[])))

  for name in AuthorList:
      instindex = []
      for inst in Authors[name].institution:
          instindex.append(InstituteList.index(inst)+1)
      Authors[name].instindex = instindex

  print("%%% automatically generated from authorlist.py %%%")
  print("\\begin{flushleft}")
  for name in AuthorList:
      print("%s$^{%s}$," % (Names[name], ', '.join(map(str, Authors[name].instindex))))
  print("\\end{flushleft}")

  print("\\begin{itemize}")
  for i in range(len(InstituteList)):
       print("\\item[$^{%d}$] %s" % (i+1,Address[InstituteList[i]]))
  print("\\end{itemize}")

if __name__ == '__main__':
    main()


