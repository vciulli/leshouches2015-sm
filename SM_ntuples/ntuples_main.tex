\section{Ntuples for NNLO events produced by \texorpdfstring{\texttt{EERAD3}}{EERAD3} \texorpdfstring{\protect\footnote{G.~Heinrich, D.~Ma{\^i}tre}}{}}
\label{sec:NNLOntuples}

We study the production of Ntuples based on the program {\tt EERAD3}
which produces parton-level QCD events to calculate event shapes and
jet rates in electron-positron annihilation through to order $\alpha_s^3$.
The aim of this study is to assess the viability of Ntuples as a general
way to have NNLO results stored and made available to the experimental community.

\subsection{Introduction}

High precision calculations will be vital in the next phase(s) of the LHC
in order to be able profit from the high quality data being collected.
In order to further explore the Higgs sector and distinguish
BSM effects from higher order effects within the Standard model,
next-to-next-to leading order (NNLO) predicitions
are necessary for a number of proccesses.
However, such predictions are the results of complex calculations,
which may take a considerable amount of time and computing resources.
Running such programs for various scale choices, parton distribution functions
and sets of cuts  is a tedious, time consuming task.

For processes with multi-particle final states at NLO, one is faced with similar
problems.
A possible solution, described in detail in Ref.~\cite{Bern:2013zja},
is to store the phase space points and the corresponding matrix elememt weights,
together with other relevant information,
in {\tt Root Ntuple} files.
This has the following advantages:
\begin{enumerate}
\item the results are flexible for (tighter) cuts to be applied at a later stage,
\item scale and PDF variations can be performed without lengthy runs of  the original program,
\item it allows to make the results readily available to experimantalists.
\end{enumerate}
The Ntuple format for NLO results already has been used successfully for various
high-multiplicity processes, e.g. W+4\,jets~\cite{Bern:2011ep},
W+5\,jets~\cite{Bern:2013gka}, or H+3\,jets~\cite{Cullen:2013saa,Greiner:2015jha}.

In this note we investigate whether the Ntuple format is also a viable option to store NNLO
results. The limitation here can come only from the size of the files containing the Ntuples.
Total sizes of the order of a TeraByte have proven practical for NLO calculations.


To get a first idea of the file sizes needed to store events which allow to produce reasonably smooth
histograms for observables calculated at NNLO, we take the program {\tt EERAD3}~\cite{Ridder:2014wza} as an example.
{\tt EERAD3} is a parton level event generator which computes the QCD corrections
to event shape observables  and
jet rates in electron-positron annihilation to order $\alpha_s^3$~\cite{GehrmannDeRidder:2007hr,GehrmannDeRidder:2008ug}.
As this example does not require any parton distribution functions, and therefore
does not contain any factorization scale dependence,
it lends itself for a first study,
which can be extended to the case of hadronic collisions later.

\subsection{Ntuples at NNLO}

\subsubsection{Brief description of the {\tt EERAD3} program}

The perturbative expansion for the distribution of an
event shape observable $\Obs$ up to NNLO at the centre-of-mass energy $\sqrt{s}$
and renormalisation scale $\mu^2 = s$, with
$\alpha_s\equiv \alpha_s(\sqrt s)$,  is given by
\begin{eqnarray}
\frac{1}{\sigma_{{\rm had}}}\, \frac{\dbox\sigma}{\dbox \Obs} &=&
\left(\frac{\alpha_s}{2\pi}\right) \frac{\dbox \bar A}{\dbox \Obs} +
\left(\frac{\alpha_s}{2\pi}\right)^2 \frac{\dbox \bar B}{\dbox \Obs}
+ \left(\frac{\alpha_s}{2\pi}\right)^3
\frac{\dbox \bar C}{\dbox \Obs} + {\cal O}(\alpha_s^4)\;.
\label{eq:NNLO}
\end{eqnarray}
In Eq.~(\ref{eq:NNLO}) the event shape distribution
is normalised to the total hadronic cross section $\sigma_{\rm{had}}$.
The latter can be expanded as
\begin{equation}
  \sigma_{\rm{had}}=\sigma_0\,
\left(1+\frac{3}{2}C_F\,\left(\frac{\alpha_s}{2\pi}\right)
+K_2\,\left(\frac{\alpha_s}{2\pi}\right)^2+{\cal O}(\alpha_s^3)\,
\right) \;,
\end{equation}
where the Born cross section for $e^+e^- \to q \bar q$ is
$\sigma_0 = \frac{4 \pi \alpha}{3 s} N  e_q^2$,
assuming massless quarks.
The constant $K_2$ is given by~\cite{Chetyrkin:1996ia}
\begin{equation}
  K_2=\frac{1}{4}\left[- \frac{3}{2}C_F^2
+C_FC_A\,\left(\frac{123}{2}-44\zeta_3\right)+C_FT_RN_F\,(-22+16\zeta_3)
 \right] \;,
\end{equation}
with
$C_A = N$, $C_F = (N^2-1)/(2N)$,
$T_R = {1}/{2}$,  and $N_F$ light quark flavours.
The program {\tt EERAD3} computes the perturbative coefficients $A$, $B$ and $C$, which are
normalised to $\sigma_0$:
\begin{eqnarray}
\frac{1}{\sigma_0}\, \frac{\dbox\sigma}{d \Obs} &=&
\left(\frac{\alpha_s}{2\pi}\right) \frac{\dbox  A}{\dbox \Obs} +
\left(\frac{\alpha_s}{2\pi}\right)^2 \frac{\dbox  B}{\dbox \Obs}
+ \left(\frac{\alpha_s}{2\pi}\right)^3
\frac{\dbox  C}{\dbox \Obs} + {\cal O}(\alpha_s^4)\,.
\label{eq:NNLOsigma0}
\end{eqnarray}
$A$, $B$ and $C$ are straightforwardly related to $\bar{A}$, $\bar{B}$
and $\bar{C}$:
\begin{eqnarray}
&&\bar{A} = A\;,\;
\bar{B} = B - \frac{3}{2}C_F\,A\;,\;
\bar{C} = C -  \frac{3}{2}C_F\,B+ \left(\frac{9}{4}C_F^2\,-K_2\right)\,A
\;.\label{eq:ceff}
\end{eqnarray}
As these coefficients are computed at a renormalisation scale fixed to
the centre-of-mass energy, they
depend only on the value of the observable $\Obs$.

The QCD coupling constant evolves according to the renormalisation group
equation, which reads to NNLO:
\begin{equation}
\label{eq:running}
\mu^2 \frac{\dbox \alpha_s(\mu)}{\dbox \mu^2} = -\alpha_s(\mu)
\left[\beta_0 \left(\frac{\alpha_s(\mu)}{2\pi}\right)
+ \beta_1 \left(\frac{\alpha_s(\mu)}{2\pi}\right)^2
+ \beta_2 \left(\frac{\alpha_s(\mu)}{2\pi}\right)^3
+ {\cal O}(\alpha_s^4) \right]\,
\end{equation}
The coefficients $\beta_i$ can be found e.g. in \cite{Ridder:2014wza}.
%with the following  coefficients in the $\overline{{\rm MS}}$-scheme:
%\begin{eqnarray}
%\beta_0 &=& \frac{11 \CA - 4 T_R \NF}{6}\;,\nonumber  \\
%\beta_1 &=& \frac{17 \CA^2 - 10 C_A T_R \NF- 6C_F T_R \NF}{6}\;, \nonumber \\
%\beta_2 &=&\frac{1}{432}
%\big( 2857 C_A^3 + 108 C_F^2 T_R N_F -1230 C_FC_A T_R N_F
%-2830 C_A^2T_RN_F \nonumber \\ &&
%+ 264 C_FT_R^2 N_F^2 + 316 C_AT_R^2N_F^2\big)\;.
%\end{eqnarray}
Eq.~(\ref{eq:running})
is solved by introducing $\Lambda$ as integration constant
with $L= \log(\mu^2/\Lambda^2)$, yielding the running coupling constant:
\begin{equation}
\alpha_s(\mu) = \frac{2\pi}{\beta_0 L}\left( 1-
\frac{\beta_1}{\beta_0^2}\, \frac{\log L}{L} + \frac{1}{\beta_0^2 L^2}\,
\left( \frac{\beta_1^2}{\beta_0^2}\left( \log^2 L - \log L - 1
\right) + \frac{\beta_2}{\beta_0}  \right) \right)\;.
\end{equation}

In terms of the running coupling $\alpha_s(\mu)$, the
NNLO (non-singlet) expression for event shape distributions therefore becomes
\begin{eqnarray}
\frac{1}{\sigma_{{\rm had}}}\, \frac{\dbox\sigma}{\dbox \Obs} (s,\mu^2,\Obs) &=&
\left(\frac{\alpha_s(\mu)}{2\pi}\right) \frac{\dbox A}{\dbox \Obs} +
\left(\frac{\alpha_s(\mu)}{2\pi}\right)^2 \left(
\frac{\dbox  B}{\dbox \Obs} + \frac{\dbox  A}{\dbox \Obs} \left[\beta_0
\log\frac{\mu^2}{s} -\frac{3}{2}\,C_F\right]\right)
\nonumber \\ &&
+ \left(\frac{\alpha_s(\mu)}{2\pi}\right)^3
\Bigg(\frac{\dbox  C}{\dbox \Obs} +  \frac{\dbox  B}{\dbox \Obs}
\left[ 2\beta_0\log\frac{\mu^2}{s}-\frac{3}{2}\,C_F\right]\nonumber\\
&&+ \frac{\dbox  A}{\dbox \Obs} \left[\beta_0^2\,\log^2\frac{\mu^2}{s}
+ \beta_1\, \log\frac{\mu^2}{s}  -3\beta_0 C_F \, \log\frac{\mu^2}{s}-K_2+\frac{9}{4}C_F^2\right]\Bigg)
\nonumber \\ &&
 + {\cal O}(\alpha_s^4)\;.
\label{eq:NNLOmu}
\end{eqnarray}
The program {\tt EERAD3} computes the perturbative coefficients
$A$, $B$ and $C$ defined in eq.~(\ref{eq:NNLOsigma0}), where the renormalisation scale
has been fixed to the centre-of-mass energy.
In order to be able to perform variations of the renormalization scale,
it is therefore sufficient to store the coefficients $A$, $B$ and $C$
in the Ntuples and apply Eq.~(\ref{eq:NNLOmu}) at the level of the Ntuple analysis.

\subsubsection{Details about the {\tt EERAD3} Ntuples}


The input file {\tt eerad3.input} contains the field {\tt nshot3\,  nshot4\,  nshot5}.
They denote the numbers of sampling points per iteration to be used by
the Monte Carlo integrator Vegas for the 3-parton, 4-parton, 5-parton channels, respectively.
The field {\tt itmax1\, itmax2} contains the number of iterations
for the ``warm up run'' (grid construction, {\tt itmax1} iterations)
and the ``production run'' (event generation and integration, {\tt itmax2} iterations).
The total number of events in each channel is then roughly given by
{\tt nshot} $\times$ {\tt itmax2}, modulo events which do not pass the
cuts.

For the implementation of this experimentation we used the same format as for the BlackHat+Sherpa NLO Ntuples~\cite{Bern:2013zja},
removing however the unnecessary information about the partonic initial states.
Figure~\ref{fig:filesizes} shows the size of the Ntuple file as a function of the number of events.
\begin{figure}
  \begin{center}
  \includegraphics[scale=0.5]{sizes.png}
  \end{center}
\caption{File size as a function of the number of events.}\label{fig:filesizes}
\end{figure}
In Table~\ref{tab:size}, we list the size of the Ntuple files
for a million events. The Ntuples contain the NNLO
results for the sum of all colour factors, i.e. {\tt icol=0}.
Further, we used {\tt y0=1d-7, iaver=0, cutvar=1d-5}.
The event numbers given in Table~\ref{tab:size} are summed over all partonic channels.


\begin{table}
\begin{center}
\begin{tabular}{|l|c|c|}
\hline
run & size/Mevents [GB] & size/Mevents [GB] with mapping \\
\hline
LO  & 0.038 & 0.038  \\
NLO & 1.66& 0.56   \\
NNLO & 433.9 & 64.7\\
\hline
\end{tabular}
\end{center}
\caption{Sizes of the Ntuple files for various choices of the total number of events.\label{tab:size}}
\end{table}
The first column of Table~\ref{tab:size} shows the size of the Ntuple file if all phase-space information is recorded in the Ntuple.
An alternative strategy is to use the fact that there is a clear relationship between the kinematical information of a real phase-space point
(``real'' refers to the phase-space with the largest final state multiplicity)  and its subtractions.
Starting from one phase-space configuration for the real radiation one can get to all the kinematical configurations for the subtractions
using a fixed number of phase-space mappings. If we can enumerate these mappings and record the mapping label instead of the actual mapped momenta
we can save a significant amount of storage, at the cost of having to perform the mappings when the Ntuple file is analysed.
The second column of Table~\ref{tab:size} shows the size of an Ntuple file if the mapping label is recorded instead of the momenta for the subtractions.
However, apart from the increased CPU time to read an Ntuple file produced in this way there is another disadvantage:
the Ntuple is more tightly bound to the program that produced it, as the implementation of the mappings has to be provided along with the Ntuple file.

We should mention that the reconstruction of observables from the information contained in the
Ntuples is
only applicable to observables which are rotation-invariant. This is due to the fact that
the phase space in {\tt EERAD3} is constructed assuming rotational invariance.


\subsection{Summary and outlook}

We have produced Root Ntuple files based on the program {\tt EERAD3}, a partonic Monte Carl program  to calculate  event shapes and
jet rates in electron-positron annihilation up to NNLO.
We have found that the size of the Ntuple files needed to produce
reasonably smooth histograms is of the order of 2-3 TeraBytes.
Therefore it seems that Ntuples can be a convenient way to store NNLO results in hadronic collisions as well.

Apart from the obvious advantages of Ntuples to be able to perform
scale and PDF variations without having to run the full program,
there are a number of other aspects which make the use of Ntuples for
NNLO results additionally appealing:
\begin{itemize}
\item the results can be made available to experimentalists,
where they can apply different sets of cuts, PDFs, etc.
 without having to deal with the NNLO code
\item it is easy to change value of $\alpha_s(M_Z)$,
such that fits of  $\alpha_s$ could be readily performed.
%\item the output fits into the format of the fastNLO framework
\end{itemize}
The file sizes presented here are obtained using the EERAD3 phase-space generator that was designed to optimize the CPU time.
One can easily imagine that, if the focus was shifted to optimise the storage space, improvements for the file size could be achieved.

We are looking forward to the first NNLO results in hadronic collisions being available in the form of Ntuples.

%comments about things to be improved in the EERAD3 program:\\
%option ``NLO only'' in eerad3\_dist is not possible at the moment\\
%improve  PS generator, based od pp to dijet experience?

